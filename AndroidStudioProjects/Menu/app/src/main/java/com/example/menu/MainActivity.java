package com.example.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String []data={"張朝旭\n職位:副教授 兼 圖書館系統管理組長\n" +
            "聯絡電話:037-381520","陳宇佐\n職位:助理教授\n" +
            "聯絡電話:037-381513","馬麗菁\n職位:教授 兼 管理學院院長\n" +
            "聯絡電話:037-381510"};
    int []pic={R.drawable.teacher01,R.drawable.teacher02,R.drawable.teacher03};
    ListView listv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listv=(ListView)findViewById(R.id.listv);
        BaseAdapter basea=new BaseAdapter() {
            @Override
            public int getCount() {
                return data.length;
            }

            @Override
            public Object getItem(int i) {
                return i;
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                View layout=View.inflate(MainActivity.this,R.layout.layout,null);
                ImageView img=(ImageView)layout.findViewById(R.id.imgv);
                TextView textv=(TextView)layout.findViewById(R.id.textv);
                img.setImageResource(pic[i]);
                textv.setText(data[i]);
                return layout;
            }
        };
        listv.setAdapter(basea);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);     //產生選單並呼叫你的menu 作為選單樣式
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();   //取得menu裡的item 的id
        // 根據取得的id判斷要做甚麼事
        if (id == R.id.item) {
            Toast.makeText(MainActivity.this,"A",Toast.LENGTH_SHORT).show();
        }
        else if (id == R.id.item5) {
            Toast.makeText(MainActivity.this,"D",Toast.LENGTH_SHORT).show();
        }
        else if (id == R.id.item2) {
            Toast.makeText(MainActivity.this,"B",Toast.LENGTH_SHORT).show();
        }
        else if (id == R.id.item3) {
            Toast.makeText(MainActivity.this,"C",Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
