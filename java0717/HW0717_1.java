package javapratice0715;
import java.util.*;
public class HW0717_1 {
	public static abstract class Animal{
		String name;
		public Animal() {
			name="Animal";
		}
		public void setName(String n) {
			name=n;
		}
		public abstract void move();
		public abstract void sound();
	}
//	public static class B extends Animal{
//		public void move() {
//			Object ob=new Object() {
//				public String toString() {
//					return "Move : walk";
//				}
//			};
//			System.out.println(ob);
//		}
//	}
	public static void main(String[] args) {
		// TODO 自動產生的方法 Stub
		Animal b=new Animal() {
			public void move() {
				System.out.println("Move: walk");
			}
			public void sound() {
				System.out.println("Sound: meow");
			}
		};
		b.move();
		b.sound();
	}
}

