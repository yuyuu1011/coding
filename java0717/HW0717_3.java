package javapratice0715;
import java.util.*;
class Account implements Comparable<Account>{
	public int balance;
	public String name;
	public Account(String b,int a) {
		this.balance=a;
		this.name=b;
	}
	@Override
	public String toString() {
		return String.format("Account(%s,%d)",name,balance);
	}
	//@Override
	public int compareTo(Account o) {
		return this.balance - o.balance;
	}
}
public class HW0717_3 {
//	public static class Account implements Comparator<Account>{
//		protected int balance;
//		protected String name;
//		public Account(int a,String b) {
//			this.balance=a;
//			this.name=b;
//		}
//		@Override
//		public String toString() {
//			return String.format("Account(%d,%s)",balance,name);
//		}
//		@Override
//		public int compareTo(Account a) {
//			return balance - a.balance;
//		}
//	}
	public static void main(String[] args) {
		// TODO 自動產生的方法 Stub
		List<Account> ac=Arrays.asList(
				new Account("A",30),
				new Account ("B",78),
				new Account ("C",66),
				new Account ("D",1200),
				new Account ("E",18)
		);
		Collections.sort(ac);
		System.out.println(ac);
		ac.sort( Comparator.<Account, String>comparing(p -> p.name) );
	    System.out.println(ac);
	}

}
