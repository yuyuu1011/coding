package javapratice0715;
import java.util.*;
public class HW01 {

	public static void main(String[] args) {
		// TODO 自動產生的方法 Stub
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
	    int i,j,k;
	    if(n>0) {
	        for(i = 1 ; i<=n ; i++)//層數的for迴圈
	        {
	            for( j = 1 ; j <=n-i ; j++)//根據外層行號，輸出星號左邊空格
	                System.out.print(" ");
	            for(k = 1 ; k <=i ; k++)//根據外層行號，輸出星號個數
	                System.out.print("*");
	            System.out.println();
	        }
	    }
	    else {
	    	System.out.println("error.");
	    }
	}

}
