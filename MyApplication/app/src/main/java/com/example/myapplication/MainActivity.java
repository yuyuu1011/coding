package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    TextView out1,out2;
    Button bt1,bt2,bt3,bt4,bt5,bt6,bt7,bt8,bt9,bt0,btclean,btplu,btmi,btmu,btdi,btans;
    String all="",muu="",num;
    String [] sp;
    boolean cl=true;
    static ArrayList<String> aall=new ArrayList<String>();
    static int nums;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Use();
        creall();
    }
    public void Use(){
      bt1=(Button)findViewById(R.id.bt1);
      bt2=(Button)findViewById(R.id.bt2);
      bt3=(Button)findViewById(R.id.bt3);
      bt4=(Button)findViewById(R.id.bt4);
      bt5=(Button)findViewById(R.id.bt5);
      bt6=(Button)findViewById(R.id.bt6);
      bt7=(Button)findViewById(R.id.bt7);
      bt8=(Button)findViewById(R.id.bt8);
      bt9=(Button)findViewById(R.id.bt9);
      bt0=(Button)findViewById(R.id.bt0);
      btclean=(Button)findViewById(R.id.btcl);
      btplu=(Button)findViewById(R.id.btp);
      btmi=(Button)findViewById(R.id.btmi);
      btmu=(Button)findViewById(R.id.btm);
      btdi=(Button)findViewById(R.id.btd);
      btans=(Button)findViewById(R.id.btans);
      out1=(TextView)findViewById(R.id.out1);
      out2=(TextView)findViewById(R.id.out2);
    }
    public void creall(){
      bt1.setOnClickListener(this);
      bt2.setOnClickListener(this);
      bt3.setOnClickListener(this);
      bt4.setOnClickListener(this);
      bt5.setOnClickListener(this);
      bt6.setOnClickListener(this);
      bt7.setOnClickListener(this);
      bt8.setOnClickListener(this);
      bt9.setOnClickListener(this);
      bt0.setOnClickListener(this);
      btclean.setOnClickListener(this);
      btplu.setOnClickListener(this);
      btmi.setOnClickListener(this);
      btmu.setOnClickListener(this);
      btdi.setOnClickListener(this);
      btans.setOnClickListener(this);
      out1.setOnClickListener(this);
      out2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.bt1:
                all+="1";
                out1.setText(all);
                aall.add("1");
                break;
            case R.id.bt2:
                all+="2";
                out1.setText(all);
                aall.add("2");
                break;
            case R.id.bt3:
                all+="3";
                out1.setText(all);
                aall.add("3");
                break;
            case R.id.bt4:
                all+="4";
                out1.setText(all);
                aall.add("4");
                break;
            case R.id.bt5:
                all+="5";
                out1.setText(all);
                aall.add("5");
                break;
            case R.id.bt6:
                all+="6";
                out1.setText(all);
                aall.add("6");
                break;
            case R.id.bt7:
                all+="7";
                out1.setText(all);
                aall.add("7");
                break;
            case R.id.bt8:
                all+="8";
                out1.setText(all);
                aall.add("8");
                break;
            case R.id.bt9:
                all+="9";
                out1.setText(all);
                aall.add("9");
                break;
            case R.id.bt0:
                all+="0";
                out1.setText(all);
                aall.add("0");
                break;
            case R.id.btp:
                all+="+";
                out1.setText(all);
                muu="plus";
                aall.add("+");
                break;
            case R.id.btmi:
                all+="-";
                out1.setText(all);
                muu="mi";
                aall.add("-");
                break;
            case R.id.btm:
                all+="*";
                out1.setText(all);
                muu="mul";
                aall.add("*");
                break;
            case R.id.btd:
                all+="/";
                out1.setText(all);
                muu="div";
                aall.add("/");
                break;
            case R.id.btcl:
                all="";
                out1.setText("");
                out2.setText("");
                aall.clear();
                break;

            case R.id.btans:
                num=out1.getText().toString();
                Log.e("num1",num);
                sp=num.split("\\+|-|\\*|/");
                Log.e("num11",sp[0]);
                Log.e("num111",sp[1]);

                int numm1=0,numm2=0;
                numm1 = Integer.parseInt(sp[0]);
                numm2 = Integer.parseInt(sp[1]);
//                for(int i=0;i<sp.length;i++){
//                    if(i==(sp.length-1)){
//                        numm1 = Integer.parseInt(sp[i]);
//                    }
//                    else {
//                        numm1 = Integer.parseInt(sp[i]);
//                        numm2 = Integer.parseInt(sp[i + 1]);
//                    }
//                }
                Log.e("aall",aall.toString());
                for(int i=0;i<aall.size();i++){
                    Log.e("i",Integer.toString(i));
                    if(aall.get(i).equals("*")){
                        int b=aall.indexOf("*");
                        int num1=Integer.valueOf(aall.get(b-1)),num2=Integer.valueOf(aall.get(b+1));
                        nums=num1*num2;
                        Log.e("*",Integer.toString(nums));
                        aall.set(b+1,Integer.toString(nums));
                        aall.remove(b-1);
                        aall.remove(b);
                    }
                    else if(aall.get(i).equals("/")){
                        int b=aall.indexOf("/");
                        int num1=Integer.valueOf(aall.get(b-1)),num2=Integer.valueOf(aall.get(b+1));
                        nums=num1/num2;
                        Log.e("/",Integer.toString(nums));
                        aall.set(b+1,Integer.toString(nums));
                        aall.remove(b-1);
                        aall.remove(b);
                    }
                }

                for(int i=0;i<aall.size();i++){
                    Log.e("i",Integer.toString(i));
                    if(aall.get(i).equals("+")){
                        int b=aall.indexOf("+");
                        int num1=Integer.valueOf(aall.get(b-1)),num2=Integer.valueOf(aall.get(b+1));
                        nums=num1+num2;
                        Log.e("+",Integer.toString(nums));
                        aall.set(b+1,Integer.toString(nums));
                        aall.remove(b-1);
                        aall.remove(b);
                    }
                    else if(aall.get(i).equals("-")){
                        int b=aall.indexOf("-");
                        int num1=Integer.valueOf(aall.get(b-1)),num2=Integer.valueOf(aall.get(b+1));
                        nums=num1-num2;
                        Log.e("-",Integer.toString(nums));
                        aall.set(b+1,Integer.toString(nums));
                        aall.remove(b-1);
                        aall.remove(b);
                    }
                }
                out2.setText(nums);

//                if(numm1!=0&&numm2!=0){
//                    all+="=";
//                    out1.setText(all);
//                    if(muu.equals("plus")){
//                        String ansfi;
//                        ansfi=Integer.toString(numm1+numm2);
//                        out2.setText(ansfi);
//                    }
//                    if(muu.equals("mi")){
//                        String ansfi;
//                        ansfi=Integer.toString(numm1-numm2);
//                        out2.setText(ansfi);
//                    }
//                    if(muu.equals("mul")){
//                        String ansfi;
//                        ansfi=Integer.toString(numm1*numm2);
//                        out2.setText(ansfi);
//                    }
//                    if(muu.equals("div")){
//                        String ansfi;
//                        ansfi=Integer.toString(numm1/numm2);
//                        out2.setText(ansfi);
//                    }
//                }

                break;

        }
    }
}
