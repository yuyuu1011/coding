package com.example.listview_spanner;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    ListView listv;
    ListAdapter a;
    String []datas={"1","2","3","4","5"};

    Spinner sp;
    String []data2={"a","b","c","d","e"};
    TextView textv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listv=(ListView)findViewById(R.id.listv);
        final AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);


        a=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,datas);
        listv.setAdapter(a);
        listv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                builder.setTitle("你選的是:");
                builder.setMessage(datas[i]);
                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog dialog=builder.create();
                dialog.show();
            }
        });

        textv=(TextView)findViewById(R.id.textv);
        sp=(Spinner)findViewById(R.id.sp);
        ArrayAdapter<String>datalist =new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,data2);
        sp.setAdapter(datalist);

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                textv.setText(data2[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                textv.setText("Nothing");
            }
        });
    }
}
