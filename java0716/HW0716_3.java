package javapratice0715;
import java.util.*;

class BigNumber{
	public BigNumber(String b,String c) {
		//n=Long.valueOf(b);
		n=b; ans=c;
	  }
	public String n,ans;
	public String plus(){
		if(n.length()>ans.length()){
			String t=n;
			n=ans;
			ans=t;
		}
		int cha=ans.length()-n.length();
		for (int i = 0; i < cha; i++) {
			n='0'+n;   //把s2前面的空缺補上0
		}
		String result="";
		int w=0;     //要進位的數 
		for (int i = ans.length()-1; i >=0 ; i--) {
			//從n和ans中取出字元，減去48就是 int型別的數字，再加上進位就是當前位的結果
			int  c=ans.charAt(i)+n.charAt(i)-96+w;
			w=c/10;    //把當前計算結果整除10就是  w的進位
			result=(c%10) + result;        //取餘就是 當前位應該顯示的數字，把它加在前面就可以了
		}	
		//因為我們的迴圈沒有判斷第一位的進位	，所以最後再判斷一次
		if(w==1)result=1+result;
		return "相加結果為:"+result;
	}
	public String minus(){
//		if(Long.valueOf(n)>Long.valueOf(ans)) {
//			long s=Long.valueOf(n)-Long.valueOf(ans);
//			String anss="相減結果為:"+Long.toString(s);
//			return anss;
//		}
//		else {
//			long s=Long.valueOf(ans)-Long.valueOf(n);
//			String anss="相減結果為:"+Long.toString(s);
//			return anss;
//		}
		int l1 = n.length();
		int l2 = ans.length();
		//判斷，如果s1小於s2那麼 符號就是  -     然後，s1和s2交換位置，保證s1是大的
		if ((l1 == l2 && n.compareTo(ans) < 0) || l1 < l2) {
			String t = n;
			n = ans;
			ans = t;
		} 
		for (int i = 0; i < Math.abs(l1-l2); i++) {
			ans='0'+ans;  //補0處理，使長度一致
		}
		String result="";
		int w=0;
		for (int i = n.length()-1; i >=0 ; i--) {
			//計算每一個位置的差，在加上借位w的值
			int c=n.charAt(i)-ans.charAt(i)+w;
			//如果c小於0，說明需要借位，c+=10，然後w該為-1，否則，借位w=0
			if(c<0){
				c+=10;
				w=-1;
			}else{
				w=0;
			}
			result=c+result;    // 把當前位的數字放入result裡
		}
		return "相減結果為:"+result;
	}
}
public class HW0716_3 {
	static ArrayList <String>arr=new ArrayList<String>();
	public static void main(String[] args) {
		// TODO 自動產生的方法 Stub
		Scanner sc=new Scanner(System.in);
		
		String n1=sc.next();
		String n2=sc.next();
		
		arr.add(n1);
		arr.add(n2);
		BigNumber b7=new BigNumber(arr.get(0),arr.get(1));
		System.out.println(b7.plus());
		System.out.println(b7.minus());
	}

}
