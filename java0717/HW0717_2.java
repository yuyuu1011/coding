package javapratice0715;
import java.util.*;
public class HW0717_2 {
	
	public static class Account{
		public Account() {
			balance=10000;
		}
		protected int balance;
		public void credit(int a) {
			balance+=a;
			System.out.println("總共有"+balance+"元");
		}
		public void debit(int a) {
			balance-=a;
			System.out.println("總共有"+balance+"元");
		}
	}
	public static class SavingAccount extends Account{
		public double interRate;
		public SavingAccount() {
			//super(1458977);
			interRate=0.1;
		}
		public double calculateInterest() {
			double sum=balance*(1+interRate);
			System.out.println("利息:"+interRate);
			return sum;
		}
	}
	public static class CheckAccount extends Account{
		public double transactionFree;
		public CheckAccount() {
			//super(1458977);
			transactionFree=0;
		}
		public void credit(int a) {
			transactionFree=chargeFee(a);
			balance=balance+a-(int)transactionFree;
			System.out.println("總共有"+balance+"元");
		}
		public void debit(int a) {
			transactionFree=chargeFee(a);
			balance=balance-a-(int)transactionFree;
			System.out.println("總共有"+balance+"元");
		}
		public double chargeFee(int a) {
			double fee=a*0.01;
			return fee;
		}
	}
	public static void main(String[] args) {
		// TODO 自動產生的方法 Stub
		Scanner sc=new Scanner(System.in);
		Account a=new Account();
		int n;
		System.out.println("-----父類別Account-----");
		System.out.println("你今天想存多少錢?");
		n=sc.nextInt();
		a.credit(n);
		System.out.println("你今天想拿多少錢?");
		n=sc.nextInt();
		a.debit(n);
		System.out.println("----------------------");
		System.out.println("-----子類別SavingAccount-----");
		SavingAccount sa=new SavingAccount();
		System.out.println("你的儲蓄有"+sa.calculateInterest()+"元");
		System.out.println("----------------------");
		System.out.println("-----子類別CheckAccount-----");
		CheckAccount ca=new CheckAccount();
		System.out.println("你今天想存多少錢?");
		n=sc.nextInt();
		ca.credit(n);
		System.out.println("你今天想拿多少錢?");
		n=sc.nextInt();
		ca.debit(n);
		System.out.println("----------------------");
	}

}
