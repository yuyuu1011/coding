package java_0722_thread;

public class HW_0722_600thread2 {
	 static class C{
		 int n;
	}
	 static C c=new C();
//	 static int n2=0;
//	 static class thread2 extends Thread{
////		 C c=new C();
//		 public void run() {
//				for(int i=0;i<600;i++) {
//					c.n++;
//					System.out.println(this.getName()+":"+c.n);
//				}
//			}
//	 }
	 
	public static void main(String[] args) {
		// TODO 自動產生的方法 Stub
		 
		Thread t=new Thread() {
			public void run() {
				synchronized(HW_0722_600thread2.class) {
					for(int i=0;i<600;i++) {
						c.n++;
						System.out.println(this.getName()+":"+c.n);
					}
				}
			}
		};
		t.start();
		Thread t2=new Thread() {
			public void run() {
				synchronized(HW_0722_600thread2.class) {
					for(int i=0;i<600;i++) {
						c.n++;
						System.out.println(this.getName()+":"+c.n);
					}
				}
			}
		};
		t2.start();
//		thread2 t=new thread2();
//		thread2 t2=new thread2();
//		t.start();
//		t2.start();
	}
	

}
