package javapratice0715;
import java.util.*;
public class HW03 {

	public static void main(String[] args) {
		// TODO 自動產生的方法 Stub
		Scanner sc=new Scanner(System.in);
		//String n=sc.nextLine();
		int n=sc.nextInt();
		int [] a=new int [n];
		for(int i=0;i<n;i++) {
			a[i]=(int)(Math.random()*100)+1;
		}
		for(int i=0;i<n;i++) {
			System.out.print(a[i]+" ");
		}
		System.out.println();
		int[] temp = ra(a);// 直接使用陣列實現反轉 
		for (int j = 0; j < temp.length; j++  ) { 
		System.out.print(temp[j]+" "); 
		} 
		
	}
	public static int[] ra(int[] A) { 
		int[] new_array = new int[A.length]; 
		for (int i = 0; i < A.length; i++ ) { 
			// 反轉後陣列的第一個元素等於源陣列的最後一個元素： 
			new_array[i] = A[A.length - i - 1]; 
		} 
		return new_array; 
	}  

}
