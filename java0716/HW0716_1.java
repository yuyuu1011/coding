package javapratice0715;
import java.util.*;
public class HW0716_1 {
	static HashMap<String, Integer>  hm=new HashMap<String, Integer>();
	static HashMap<String, String>  hmns=new HashMap<String,String>();
	public static void main(String[] args) {
		// TODO 自動產生的方法 Stub
		Scanner sc=new Scanner(System.in);
		HW0716_1 h=new HW0716_1();
		address ad=h.new address();
		ad.add();
		System.out.println("有台北101、東京迪士尼、法國艾菲爾鐵塔、美國黃石國家公園，請輸入兩個地點");
		String ans=sc.next();
		String ans2=sc.next();
		ad.time(ans, ans2);
		System.out.println("請輸入一個地點來查詢經緯度");
		String ans3=sc.next();
		ad.news(ans3);
	}
	class address{
		public void add() {
			hm.put("台北101", 8);
			hm.put("東京迪士尼", 9);
			hm.put("法國艾菲爾鐵塔", 1);
			hm.put("美國黃石國家公園", -6);
			hmns.put("台北101", "35°37N,139°52E");
			hmns.put("東京迪士尼", "25°02N,121°33E");
			hmns.put("法國艾菲爾鐵塔", "48°51N,2°17E");
			hmns.put("美國黃石國家公園", "44°36N,110°30W");
		}
		public void time(String x,String y) {
			int a=hm.get(x);
			int b=hm.get(y);
			int c;
			if(a>b) {
				c=a-b;
			}
			else {
				c=b-a;
			}
			System.out.println("時間差為:"+c);
		}
		public void news(String a) {
			String ns=hmns.get(a);
			System.out.println(ns);
		}
	}

}
